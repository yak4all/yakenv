# YakEnv

This project is responsible for providing the development tools used by YaK Core, including Ansible, AWS, OCI, Azure specific tools, etc. With each new build of the YaK Core container, it ensures the integration of the latest versions of the binaries.

## Building a YakEnv BETA Package

When building a YakEnv beta package with version 1.7.3.beta, it will automatically create a beta package that is seamlessly utilized by YaK Core.

## Building a YakEnv STABLE Package

To build a YakEnv stable package, it's essential to have the corresponding beta package already available. For a stable package, no new package is generated from the code; instead, a new package is copied from an existing beta package.

This process enables us to conduct tests on specific binary versions during one month on beta and subsequently release a new stable version without modifying the code. This ensures that the stable version is 100% identical to the beta version.

For example, if you have a 1.7.3-beta to create a new stable YakEnv container, you must create a tag 1.7.3-stable.
