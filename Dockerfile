FROM debian:11.11

RUN  apt-get update \
     && apt-get dist-upgrade -y \
     && mkdir -p /dev /usr/share/ansible/collections \
     && apt-get install bash sudo git wget curl unzip python3-pip python3-netaddr jq vim tree iputils-ping traceroute dnsutils nano -y \
     && echo "# ----------"\
     && echo "# 1. Ansible"\
     && echo "# ----------"\
     && pip3 install ansible \
     && pip3 install ansible-runner \
     && pip3 install ruamel.yaml \ 
     && echo "# --------------------------------"\
     && echo "# 2. Oracle OCI Ansible collection"\
     && echo "# --------------------------------"\
     && mkdir -p /etc/ansible/collections \
     && ansible-galaxy collection install oracle.oci --collections-path /etc/ansible/collections \
     && pip3 install -r /etc/ansible/collections/ansible_collections/oracle/oci/requirements.txt \
     && echo "# ---------------------------"\
     && echo "# 3. Azure Ansible Collection"\
     && echo "# ---------------------------"\
     # Force installation of the collection as it is already preinstalled with a lower version
     && ansible-galaxy collection install azure.azcollection --collections-path /etc/ansible/collections --force \
     && pip3 install -r /etc/ansible/collections/ansible_collections/azure/azcollection/requirements.txt \
     && echo "# ---------------------------------"\
     && echo "# 4. Install Windows Remote Manager"\
     && echo "# ---------------------------------"\
     && pip3 install pywinrm \
     && echo "# ------------------"\
     && echo "# 5. Oracle OCI-CLI"\
     && echo "# ------------------"\
     && pip3 install oci-cli \
     && echo "# ----------"\
     && echo "# 6. AWS CLI"\
     && echo "# ----------"\
     && pip3 install boto3 \
     && pip3 install boto \
     # && echo "# ----------"\
     # && echo "# pip3 install ansible-lint"\
     # && echo "# ----------"\
     # && pip3 install ansible-lint \
     && echo "# ----------"\
     && echo "# pip3 install request"\
     && echo "# ----------"\
     && pip3 install requests \
     && curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" \
     && unzip awscliv2.zip \
     && ./aws/install \
     && echo "# ------------"\
     && echo "# 7. Azure CLI"\
     && echo "# ------------"\
     && curl -sL https://aka.ms/InstallAzureCLIDeb -o "azurecli.sh" \
     && chmod +x azurecli.sh \
     && sudo ./azurecli.sh \
     && echo "# ----------"\
     && echo "# 8. Cleanup"\
     && echo "# -----------"\
     && rm -f *.zip
